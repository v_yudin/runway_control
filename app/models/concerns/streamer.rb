class Streamer
  STREAM_CHANNEL = 'stream'

  def self.message(message)
    Redis.new.publish(STREAM_CHANNEL, message)
  end

  def self.message_state_changed(plane)
    "state_changed:#{plane.id}:#{plane.state}:#{plane.state.humanize}"
  end
end
