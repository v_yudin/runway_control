class Plane < ActiveRecord::Base
  before_update :before_update_validation, if: :state_changed?
  after_update  :on_update
  after_create  :on_create
  after_destroy :on_destroy

  has_secure_token

  enum state: [
    :awaiting,
    :pending,
    :in_progress,
    :took_off
  ]

  validates :number, length: { minimum: 3, maximum: 32 }, format: { with: /\A[\wа-яё]+\z/i }, uniqueness: true
  validates :state, inclusion: { in: states.keys }

  has_many :logs

  scope :ordered, -> { order(:number) }

  def takeoff
    unless awaiting?
      errors.add :base, I18n.t('plane.errors.already_in_progress')
      return false
    end

    self.state = self.class.in_progress.present? ? :pending : :in_progress
    save
  end

  def cancel
    unless in_progress? || pending?
      errors.add :base, I18n.t('plane.errors.cannot_cancel')
      return false
    end

    self.state = :awaiting
    save
  end

  def confirm_takeoff(token)
    unless in_progress?
      errors.add :base, I18n.t('plane.errors.not_in_progress')
      return false
    end

    unless self.token == token
      errors.add :base, I18n.t('plane.errors.invalid_credentials')
      return false
    end

    self.state = :took_off
    save
  end

  private

  def before_update_validation
    if state == 'in_progress' && self.class.in_progress.present?
      self.errors.add :base, I18n.t('plane.errors.in_progress_is_already_present')
      return false
    end

    self.state_changed_at = DateTime.now
  end

  def on_update
    if state_changed?
      Log.create plane: self, event: :state_changed, state: state
      Streamer.message(Streamer.message_state_changed(self))
      self.class.process_pending
    end
  end

  def on_create
    Log.create plane: self, event: :plane_created
  end

  def on_destroy
    Log.create plane: self, event: :plane_destroyed
    self.class.process_pending if in_progress?
  end

  def self.process_pending
    return if self.in_progress.present?

    pending_plane = self.pending.order(:state_changed_at).first

    if pending_plane
      pending_plane.in_progress!
    end
  end
end
