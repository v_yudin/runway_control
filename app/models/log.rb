class Log < ActiveRecord::Base
  enum state: [
    :awaiting,
    :pending,
    :in_progress,
    :took_off
  ]

  enum event: [
    :state_changed,
    :plane_created,
    :plane_destroyed
  ]

  validates :state, allow_blank: true, inclusion: { in: states.keys }
  validates :event, inclusion: { in: events.keys }

  belongs_to :plane

  scope :ordered, -> { order(created_at: :desc) }
end
