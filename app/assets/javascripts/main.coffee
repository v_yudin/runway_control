@show_message = (message_type, text = 'Unexpected error') ->
  switch message_type
    when 'success' then toastr.success text
    when 'info' then toastr.info text
    when 'warning' then toastr.warning text
    else toastr.error text
