@init_planes_page = ->
  $('.takeoff a').on 'click', (e) ->
    e.preventDefault()

    $this = $(this)

    $.ajax $this.attr('href'),
      type: 'PATCH'
    .done (data) ->
      show_message(data.message_type, data.text)
    .error (data) ->
      show_message()

  $('.cancel a').on 'click', (e) ->
    e.preventDefault()

    $this = $(this)

    $.ajax $this.attr('href'),
      type: 'PATCH'
    .done (data) ->
      show_message(data.message_type, data.text)
    .error (data) ->
      show_message()

  $('.confirm a').on 'click', (e) ->
    e.preventDefault()

    $this = $(this)

    $.ajax $this.attr('href'),
      type: 'PATCH'
      data:
        token: $this.closest('.plane').data('token')
    .done (data) ->
      show_message(data.message_type, data.text)
    .error (data) ->
      show_message()

  $('.plane.new .add a').on 'click', (e) ->
    e.preventDefault()
    create_plane()

  $('.plane.new form').on 'submit', (e) ->
    e.preventDefault()
    create_plane()

  create_plane = ->
    $plane  = $('.plane.new')

    $.ajax $plane.find('form').attr('action'),
      type: 'POST'
      data: $plane.find('form').serialize()
    .done (data) ->
      show_message(data.message_type, data.text)
      location.reload() if data.message_type == 'success'
    .error (data) ->
      show_message()

  socket = new WebSocket("ws://#{window.location.host}/stream");

  socket.onclose = (event) ->
    alert("WS connection closed. Code: #{event.code}, reason: #{event.reason} \nPlease, reload the page.")

  socket.onmessage = (event) ->
    data = event.data.split(':')

    event_name  = data[0]
    plane_id    = data[1]
    extra       = "#{data[2]}:#{data[3]}"

    process_event(event_name, plane_id, extra)

  process_event = (event, id, data) ->
    switch event
      when 'state_changed'
        $plane = $(".plane[data-id=#{id}]")
        return unless $plane?
        change_plane_state($plane, data)
      else
        # unknown event

  change_plane_state = ($plane, data) ->
    state = data.split(':')

    state_name      = state[0]
    state_humanized = state[1]

    switch state_name
      when 'in_progress'
        $plane.find('.cancel').removeClass('hidden')
        $plane.find('.takeoff').addClass('hidden')
        $plane.find('.confirm').removeClass('hidden')
        $plane.find('.state').text(state_humanized)
      when 'pending'
        $plane.find('.cancel').removeClass('hidden')
        $plane.find('.takeoff').addClass('hidden')
        $plane.find('.confirm').addClass('hidden')
        $plane.find('.state').text(state_humanized)
      when 'awaiting'
        $plane.find('.cancel').addClass('hidden')
        $plane.find('.takeoff').removeClass('hidden')
        $plane.find('.confirm').addClass('hidden')
        $plane.find('.state').text(state_humanized)
      when 'took_off'
        $plane.find('.cancel').addClass('hidden')
        $plane.find('.takeoff').addClass('hidden')
        $plane.find('.confirm').addClass('hidden')
        $plane.find('.state').text(state_humanized)
      else
        # unknown state
