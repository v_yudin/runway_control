class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include Tubesock::Hijack

  def stream
    hijack do |socket|
      handler = Thread.new do
        Redis.new.subscribe(Streamer::STREAM_CHANNEL) do |listener|
          listener.message do |_, data|
            socket.send_data(data)
          end
        end
      end

      socket.onclose { handler.kill }
    end
  end

  protected

  def render_message(type, text, options = {})
    available_types = %w(success info warning error)

    raise ArgumentError, 'unknown message type' unless available_types.include? type.to_s

    text ||= type.humanize

    render json: options.merge( { message_type: type, text: text } )
  end
end
