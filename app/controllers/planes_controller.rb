class PlanesController < ApplicationController
  before_action :find_plane, only: [:show, :destroy, :takeoff, :confirm_takeoff, :cancel]

  def index
    @planes = Plane.all.ordered
  end

  def create
    @plane = Plane.new allowed_params

    if @plane.save
      render_message :success, t('.success')
    else
      render_message :error, t('.error') + ': ' + @plane.errors[:number].join(', ')
    end
  end

  def destroy
    @plane.destroy
    redirect_to planes_path
  end

  def takeoff
    if @plane.takeoff
      render_message :success, t('planes.state.success')
    else
      render_message :error, "#{t('planes.state.error')}: #{@plane.errors[:base].join(', ')}"
    end
  end

  def cancel
    if @plane.cancel
      render_message :success, t('planes.state.success')
    else
      render_message :error, "#{t('planes.state.error')}: #{@plane.errors[:base].join(', ')}"
    end
  end

  def confirm_takeoff
    token = params[:token]

    if @plane.confirm_takeoff(token)
      render_message :success, t('planes.state.success')
    else
      render_message :error, "#{t('planes.state.error')}: #{@plane.errors[:base].join(', ')}"
    end
  end

  private

  def find_plane
    @plane = Plane.find(params[:id])
  end

  def allowed_params
    params.require(:plane).permit(:number)
  end
end
