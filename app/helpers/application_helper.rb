module ApplicationHelper
  def icon(icon)
    content_tag :i, nil, class: [:icon, icon]
  end
end
