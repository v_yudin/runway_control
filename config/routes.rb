Rails.application.routes.draw do
  resources :planes do
    patch :confirm_takeoff, on: :member
    patch :cancel, on: :member
    patch :takeoff, on: :member

    get :logs, on: :collection
  end

  get :stream, to: 'application#stream'

  root to: 'planes#index'
end
