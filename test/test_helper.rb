ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "minitest/rails"
require "database_cleaner"

#minitest coloured output
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods
  ActiveRecord::Migration.check_pending!
end

class Minitest::Spec
  include FactoryGirl::Syntax::Methods
end

DatabaseCleaner.strategy = :truncation
DatabaseCleaner.clean
