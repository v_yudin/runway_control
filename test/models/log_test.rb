require './test/test_helper'

describe Plane do
  let(:log) { create :log }

  it 'must be valid' do
    log.must_be :valid?
  end

  it 'can have an unset state' do
    log.state = nil
    log.must_be :valid?
  end

  it 'should have a set event' do
    log.event = nil
    log.wont_be :valid?
  end
end
