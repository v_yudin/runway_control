require './test/test_helper'

describe Plane do
  let(:plane) { create :plane }

  it 'must be valid' do
    plane.must_be :valid?
  end

  it 'must be invalid without number' do
    plane.number = nil
    plane.wont_be :valid?
  end

  it 'must be invalid if number is less than 3 symbols' do
    plane.number = 'AA'
    plane.wont_be :valid?
  end

  it "must be invalid if number doesn't match the regexp" do
    plane.number = 'TEST !@$'
    plane.wont_be :valid?
  end

  it 'must be invalid if number exists' do
    plane2        = create :plane, number: 'TEST'
    plane2.number = plane.number

    plane2.wont_be :valid?
  end

  it 'should have a set state' do
    plane.state = nil
    plane.wont_be :valid?
  end

  it 'should change state on takeoff if awaiting' do
    plane.takeoff
    plane.reload.state.must_equal 'in_progress'
  end

  it 'should not be valid if not awaiting' do
    plane.state = :pending
    plane.takeoff
    plane.errors[:base].must_be :present?
  end

  it 'should change state on cancel if in progress or pending' do
    plane.state = :in_progress
    plane.cancel
    plane.reload.state.must_equal 'awaiting'
  end

  it 'should not be valid if awaiting' do
    plane.cancel
    plane.errors[:base].must_be :present?
  end

  it 'should change state on confirm if in progress' do
    plane.state = :in_progress
    plane.confirm_takeoff(plane.token)
    plane.reload.state.must_equal 'took_off'
  end

  it 'should not be valid if not in progress' do
    plane.confirm_takeoff(plane.token)
    plane.errors[:base].must_be :present?
  end

  it 'should not be valid if token is invalid' do
    plane.confirm_takeoff(plane.token + 'INVALID')
    plane.errors[:base].must_be :present?
  end
end
