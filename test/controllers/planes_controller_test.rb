require './test/test_helper'

describe PlanesController do
  before do
    @plane = create :plane
  end

  it 'must be successful' do
    get :index
    response.must_be :success?
  end

  describe 'POST create' do
    subject { xhr :post, :create, plane: { number: 'NUMBER' } }

    it 'must return success' do
      subject
      json = JSON.parse(response.body)
      json['message_type'].must_equal 'success'
    end

    it 'must return error if number is invalid' do
      xhr :post, :create, plane: { number: 'INVALID !@$' }
      json = JSON.parse(response.body)
      json['message_type'].must_equal 'error'
    end

    it 'must return error if number is not unique' do
      xhr :post, :create, plane: { number: @plane.number }
      json = JSON.parse(response.body)
      json['message_type'].must_equal 'error'
    end
  end

  describe 'DELETE destroy' do
    it 'must redirect to the planes_path' do
      delete :destroy, id: @plane.id
      assert_redirected_to planes_path
    end
  end

  describe 'PATCH takeoff' do
    it 'must change state to In progress if there are no planes in progress' do
      xhr :patch, :takeoff, id: @plane.id
      @plane.reload.state.must_equal 'in_progress'
    end

    it 'must change state to Pending if there are planes in progress' do
      create :plane, number: 'test', state: 'in_progress'
      xhr :patch, :takeoff, id: @plane.id
      @plane.reload.state.must_equal 'pending'
    end

    it 'must not change state if plane is already pending' do
      @plane.state = 'pending'
      @plane.save

      xhr :patch, :takeoff, id: @plane.id
      json = JSON.parse(response.body)
      json['message_type'].must_equal 'error'
    end
  end
end
