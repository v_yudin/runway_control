class CreatePlanes < ActiveRecord::Migration
  def change
    create_table :planes do |t|
      t.string :number, null: false
      t.integer :state, null: false, default: 0
      t.datetime :state_changed_at
      t.string :token
      t.integer :lock_version, null: false, default: 0

      t.timestamps null: false
    end
  end
end
