class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.integer :event, null: false
      t.integer :state
      t.references :plane

      t.timestamps null: false
    end
  end
end
